import java.util.Scanner;
public class Driver{
	public static void main(String[] args){
		add();
		sqrt();
		System.out.println("Here's a random number: " + Calculator.random());
	}
	
	public static void add(){
		Scanner reader = new Scanner(System.in);
		System.out.println("Hello! Type a number you would like to add");
		
		int x = reader.nextInt();
		reader.nextLine();
		
		System.out.println("Type another number you would like to add");
		
		int y = reader.nextInt();
		reader.nextLine();
		
		int result = Calculator.add(x, y);
		
		System.out.println("Here's your sum: " + result);
	}
	
	public static void sqrt(){
		Scanner reader = new Scanner(System.in);
		System.out.println("Type a number that you would like to square root");
		
		int x = reader.nextInt();
		reader.nextLine();
		
		double result = Calculator.sqrt(x);
		
		System.out.println("Here's your result: " + result);
	}
}