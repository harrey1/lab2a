import java.util.Random;
public class Calculator{
	public static int add(int x, int y){
		return x + y;
	}
	
	public static double sqrt(int x){
		double y = Math.sqrt(x);
		return y;
	}
	
	public static int random(){
		Random ran = new Random();
		return ran.nextInt(100);
	}
	
	public static int divide(int x, int y){
		if (y == 0){
			return 0;
		}
		return x / y;
	}
}